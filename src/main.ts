import fastify, { FastifyRequest } from 'fastify';
import cors from '@fastify/cors';
import cookie from '@fastify/cookie';

import fs from 'fs';
import path from 'path';

import { config } from './config';
import { dbDataSource } from './modules/database.module';
import './modules/s3.module';
import redis from './modules/redis.module';

import * as Sentry from '@sentry/node';

import BadRequestException from './exceptions/badrequest.exception';
import UnauthorizedException from './exceptions/unauthorized.exception';
import ForbiddenException from './exceptions/forbidden.exception';
import HttpException from './exceptions/http.exception';

let ReqId: number = 0;

Sentry.init({
  dsn: config.sentry.dsn,
});

export const sentry = Sentry;

export const app = fastify({
  logger: config.api.dev
    ? {
        transport: {
          target: 'pino-pretty',
          options: {
            translateTime: 'HH:MM:ss Z',
            ignore: 'pid,hostname',
          },
        },
        serializers: {
          req: (req: FastifyRequest) => {
            return {
              ip: req.ip,
              endpoint: req.originalUrl,
              method: req.method,

              data: {
                body: req.body,
                headers: req.headers,
                params: req.params,
                query: req.query,
              },
            };
          },
        },
      }
    : false,
  trustProxy: true,
  genReqId: () => {
    ReqId += 1;
    return `req-${ReqId}`;
  },
  ignoreTrailingSlash: true,
  ignoreDuplicateSlashes: true,
});

app.setErrorHandler(async (error, _request, reply) => {
  if (error instanceof SyntaxError)
    return reply.status(400).send({
      error: {
        error: 'syntax',
        message: error.message,
      },
    });

  if (error.validation)
    return reply.status(400).send({
      error: {
        error: 'validation',
        message: error.validation[0].message,
      },
    });

  if (error instanceof BadRequestException)
    return reply.status(400).send({ error: error.error });

  if (error instanceof UnauthorizedException)
    return reply
      .status(401)
      .send({ error: { error: error.name, description: error.message } });

  if (error instanceof ForbiddenException)
    return reply.status(403).send({
      error: error.error,
    });

  if (error instanceof HttpException)
    return reply.status(error.status).send({ error: error.error });

  if (error.statusCode && error.statusCode >= 500) {
    sentry.captureException(error);

    return reply.status(error.statusCode).send({
      error: { error: 'server-exception', message: 'Unexpected Exception' },
    });
  }
});

app.register(cors, {
  origin: config.api.dev ? true : config.cors,
  credentials: true,
});

app.register(cookie, {
  secret: config.cookie.secret,
  hook: 'onRequest',
  parseOptions: {
    maxAge: 60 * 60 * 24 * 1000,
    secure: true,
    signed: true,
    httpOnly: true,
    domain: config.api.domain,
    path: '/',
  },
});

redis.connect().then(() => {
  app.log.info('Redis-Storage connection successful.');
});

dbDataSource
  .initialize()
  .then(() => {
    app.log.info('DB successfully initialized');
  })
  .catch((e) => {
    app.log.fatal(`Connection to DB failed with reason: ${e}`);
    process.exit(1);
  });

app.listen(
  { port: config.api.port, host: process.env.HTTP_HOST ?? 'localhost' },
  async (err) => {
    if (err) {
      app.log.fatal(`Failed to start API: ${err}`);
      process.exit(1);
    }
  }
);

redis.on('error', (e) => {
  app.log.fatal(e, 'Redis-Storage connection error.');

  process.exit(1);
});

const findControllerFiles = (folderPath: string): string[] => {
  const controllerFiles: string[] = [];

  function traverseDirectory(currentPath: string) {
    const files = fs.readdirSync(currentPath);
    files.map((file) => {
      const filePath = path.join(currentPath, file);
      const stats = fs.statSync(filePath);
      if (stats.isDirectory()) {
        traverseDirectory(filePath);
      }

      if (
        (stats.isFile() && file.endsWith('.controller.js')) ||
        file.endsWith('.controller.ts')
      ) {
        controllerFiles.push(filePath);
      }
    });
  }

  traverseDirectory(folderPath);
  return controllerFiles;
};

findControllerFiles(path.join(__dirname, 'modules')).map((controller) => {
  app.register(import(controller));
});
