import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum EUserRole {
  'ADMIN' = 1,
  'USER' = 0,
  'BANNED' = 3,
}

@Entity('users')
export class EUser {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  password!: string;

  @Column({ unique: true })
  username!: string;

  @Column({ unique: true })
  email!: string;

  @Column({ default: 0 })
  role!: EUserRole;

  isAdmin(): boolean {
    return this.role === EUserRole.ADMIN;
  }

  isBanned(): boolean {
    return this.role === EUserRole.BANNED;
  }
}
