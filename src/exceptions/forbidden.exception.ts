import { IError } from 'types';

class ForbiddenException extends Error {
  status: number;
  error: IError;

  constructor(message: IError) {
    super(message.error);

    this.status = 403;
    this.error = {
      error: message.error,
      description: message.description,
    };

    this.name = 'Forbidden';
  }
}

export default ForbiddenException;
