import { IError } from 'types';

class HttpException extends Error {
  status: number;
  error: IError;

  constructor(code: number, message: IError) {
    super(message.error);

    this.status = code;
    this.error = {
      error: message.error,
      description: message.description,
    };

    this.name = 'HttpException';
  }
}

export default HttpException;
