class UnauthorizedException extends Error {
  status: number;

  constructor() {
    super();
    this.status = 401;

    this.name = 'UnauthorizedException';
    this.message =
      'Not authenticated. You must be authorized to use this endpoint.';
  }
}
export default UnauthorizedException;
