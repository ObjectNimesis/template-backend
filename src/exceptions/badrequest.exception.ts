import { IError } from 'types';

class BadRequestException extends Error {
  status: number;
  error: IError;

  constructor(message: IError) {
    super(message.error);

    this.status = 400;
    this.error = {
      error: message.error,
      description: message.description,
    };

    this.name = 'BadRequestError';
  }
}

export default BadRequestException;
