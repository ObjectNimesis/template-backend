import c from 'dotenv';

const isDev = process.env.NODE_ENV === 'develoment';
if (isDev) c.config({ path: '.env.dev' });

export const config = {
  api: {
    port: Number(process.env.HTTP_PORT ?? 4000),
    domain: process.env.API_DOMAIN,
    dev: isDev,
  },
  jwt: {
    secret: process.env.JWT_SECRET as string,
  },
  sentry: {
    dsn: process.env.SENTRY_SDN_URL,
  },
  cookie: {
    secret: process.env.COOKIE_SECRET,
  },
  s3: {
    host: process.env.S3_HOST,
    secret_key: process.env.S3_SECRET_KEY as string,
    key_id: process.env.S3_KEY_ID as string,
  },
  db: {
    type: process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    port: 3306,
    user: process.env.DB_USER,
    database: process.env.DB_PORT,
    password: process.env.DB_PASSWORD,
  },
  redis: {
    user: process.env.REDIS_USER,
    password: process.env.REDIS_PASS,
    host: process.env.REDIS_HOST,
    port: Number(process.env.REDIS_PORT),
    database: 0,
  },
  cors: process.env.CORS_URL,
};
