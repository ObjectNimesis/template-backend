import { DataSource } from 'typeorm';
import { config } from '../config';

import path from 'path';
import fs from 'fs';

import { EUser } from '../entities/user/EUser';

const getEntities = (dirPath: string, entities: string[] = []) => {
  const entries = fs.readdirSync(dirPath, { withFileTypes: true });
  for (const entry of entries) {
    const fullPath = path.join(dirPath, entry.name);

    if (entry.isDirectory()) {
      getEntities(fullPath, entities);
    } else {
      entities.push(fullPath);
    }
  }
  return entities;
};

export const dbDataSource = new DataSource({
  type: config.db.type as any,
  host: config.db.host,
  port: config.db.port,
  username: config.db.user,
  password: config.db.password,
  database: config.db.database,
  entities: [...getEntities(path.join(__dirname, '..', 'entities'))],
  synchronize: true,
});

export const repositories = {
  users: {
    accounts: dbDataSource.getRepository(EUser),
  },
};
