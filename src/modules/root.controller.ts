import { FastifyInstance } from 'fastify';
import { requireAuthentication } from 'src/middlewares/auth/auth.middleware';

import BadRequestException from 'src/exceptions/badrequest.exception';
import HttpException from 'src/exceptions/http.exception';
import UnauthorizedException from 'src/exceptions/unauthorized.exception';
import ForbiddenException from 'src/exceptions/forbidden.exception';
import { requireRoleAuthentication } from 'src/middlewares/auth/role.middleware';
import { EUserRole } from 'src/entities/user/EUser';

const router = (fastify: FastifyInstance, opts: unknown, done: () => void) => {
  fastify.get('/', async (request, reply) => {
    return reply.send({ message: 'Hello world!' });
  });

  fastify.get(
    '/users/me',
    { preHandler: requireAuthentication },
    async (request, reply) => {
      return reply.send({ message: 'Hello world!', data: request.user });
    }
  );

  fastify.get(
    '/role-protected',
    { preHandler: requireRoleAuthentication(EUserRole.ADMIN) },
    async (request, reply) => {}
  );

  fastify.get(
    '/check-is-admin',
    { preHandler: requireAuthentication },
    async (request, reply) => {
      if (request.user.isAdmin()) return reply.send({ data: true });
      return reply.send({ data: false });
    }
  );

  fastify.get('/unauthorized', async (request, reply) => {
    throw new UnauthorizedException();
  });

  fastify.get('/forbidden', async (request, reply) => {
    throw new ForbiddenException({
      error: 'x',
      description: 'y',
    });
  });

  fastify.get('/badrequest', async (request, reply) => {
    throw new BadRequestException({
      error: 'invalid_user',
      description: 'invalid user-id',
    });
  });

  fastify.get('/custom', async (request, reply) => {
    throw new HttpException(409, {
      error: 'conflict',
      description: 'conflicting user-id',
    });
  });

  done();
};

export default router;
