import { createClient } from 'redis';
import { config } from '../config';

const redis = createClient({
  password: config.redis.password,
  socket: {
    host: config.redis.host,
    port: config.redis.port,
  },
  database: config.redis.database,
});

export default redis;
