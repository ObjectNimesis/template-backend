import { FastifyInstance } from 'fastify';
import { requireAuthentication } from '../../middlewares/auth/auth.middleware';
import {
  ISignInRequestBody,
  ISingInRequestSchema,
} from './schemas/signin.schema';
import {
  ISignUpRequestBody,
  ISignUpRequestSchema,
} from './schemas/signup.schema';

const router = (fastify: FastifyInstance, opts: unknown, done: () => void) => {
  fastify.post(
    '/auth/signin',
    {
      schema: ISingInRequestSchema,
    },
    async (request, reply) => {
      const data = request.body as ISignInRequestBody;

      return reply.send({
        data: { message: 'Successfuly authorized' },
      });
    }
  );

  fastify.post(
    '/auth/signup',
    {
      schema: ISignUpRequestSchema,
    },
    async (request, reply) => {
      const data = request.body as ISignUpRequestBody;

      return reply.send({});
    }
  );

  fastify.post(
    '/auth/signout',
    { preHandler: requireAuthentication },
    async (request, reply) => {
      return reply.send({ data: { message: 'Successfully signed out' } });
    }
  );

  done();
};

export default router;
