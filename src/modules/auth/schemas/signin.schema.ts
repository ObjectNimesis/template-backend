export interface ISignInRequestBody {
  password: string;
  email: string;
}

export const ISingInRequestSchema = {
  body: {
    type: 'object',
    required: ['email', 'password'],
    properties: {
      email: { type: 'string' },
      password: { type: 'string' },
    },
  },
};
