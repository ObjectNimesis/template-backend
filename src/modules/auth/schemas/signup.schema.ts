import { ISignInRequestBody } from './signin.schema';

export interface ISignUpRequestBody extends ISignInRequestBody {
  username: string;
}

export const ISignUpRequestSchema = {
  body: {
    type: 'object',
    required: ['email', 'password', 'username'],
    properties: {
      email: { type: 'string', format: 'email' },
      password: { type: 'string' },
      login: { type: 'string' },
    },
  },
};
