import stringUtils from '../../utils/string.utils';
import argon from 'argon2';
import { FastifyReply, FastifyRequest } from 'fastify';
import { EUser } from '../../entities/user/EUser';
import { repositories } from '../database.module';
import redis from '../redis.module';
import DeviceDetector from 'node-device-detector';
import { ISignUpRequestBody } from './schemas/signup.schema';
import UnauthorizedException from 'src/exceptions/unauthorized.exception';

export interface IAuthAgent {
  device: {
    device: string;
    agent: string;
  };
  ip: string;
}

interface IAssignSession {
  user: EUser;
  agent?: IAuthAgent;
  reply: FastifyReply;
  remember?: boolean;
}

export interface ISession {
  id: string;
  user: {
    id: number;
  };
  agent: IAuthAgent;
  timestamp: string;
}

interface IRemember {
  user: {
    id: number;
  };
}

class DeautorizeSessions {
  async byId(id: string): Promise<void> {
    try {
      const keys = await redis.keys(`session:uid-*:${id}`);

      for (let i = 0; i < keys.length; i++) {
        const k = keys[i];
        await redis.del(k);
      }
    } catch (error) {}
  }

  async all(user_id: number, current?: string): Promise<void> {
    try {
      const keys = await redis.keys(`session:uid-${user_id}:*`);

      for (let i = 0; i < keys.length; i++) {
        const k = keys[i];
        if (current && k.split(':')[3] === current) continue;

        await redis.del(k);
      }
    } catch (error) {}
  }
}

class SessionsService {
  public deauthorize = new DeautorizeSessions();

  async get(id: string): Promise<ISession | undefined> {
    const keys = await redis.keys(`session:uid-*:${id}`);
    if (!keys || keys.length < 1) return undefined;
    let session: ISession | undefined = undefined;

    try {
      const raw = await redis.get(keys[0]);
      if (!raw) return;

      const parsed = JSON.parse(raw);
      session = { id, ...parsed };
    } catch (error) {}

    return session;
  }

  async list(user_id: number): Promise<ISession[]> {
    const keys = await redis.keys(`session:uid-${user_id}:*`);
    const sessions: ISession[] = [];

    for await (const k of keys) {
      try {
        const raw = await redis.get(k);
        if (!raw) continue;

        const session = JSON.parse(raw);
        sessions.push({ id: k.split(':')[3], ...session });
      } catch (error) {}
    }
    return sessions;
  }

  async assign(data: IAssignSession): Promise<void> {
    if (!data.remember) data.remember = false;

    const sid = stringUtils.getRandomString({ length: 24 });
    await redis.set(
      `session:uid-${data.user.id}:${sid}`,
      JSON.stringify({
        user: {
          id: data.user.id,
        },
        agent: data.agent,
        timestamp: Date.now(),
      }),
      {
        EX: 2629746,
      }
    );

    if (data.remember) {
      const RememberMeCode = stringUtils.getRandomString({ length: 16 });

      await redis.set(
        `remember:${RememberMeCode}`,
        JSON.stringify({
          user: {
            id: data.user.id,
          },
        }),
        {
          EX: 2629746,
        }
      );

      const exp = new Date();
      exp.setMonth(exp.getMonth() + 1);

      data.reply.setCookie('remember_me', RememberMeCode, { expires: exp });
    }

    const expires = new Date();
    expires.setMonth(expires.getMonth() + 1);

    data.reply.setCookie('DigiProxySessionId', sid, { expires });
  }
}

class AuthService {
  private DD = new DeviceDetector();
  public sessions = new SessionsService();

  public remember = {
    get: async (code: string): Promise<IRemember | undefined> => {
      const key = await redis.get(`remember:${code}`);
      if (!key) return;

      let remember: IRemember | undefined = undefined;
      try {
        remember = JSON.parse(key) as IRemember;
      } catch (error) {}

      return remember;
    },
  };

  async createUser(data: ISignUpRequestBody): Promise<EUser> {
    const entity = await repositories.users.accounts.save({
      username: data.username,
      email: data.email,
      password: data.password && (await argon.hash(data.password)),
    });

    return entity;
  }

  async logout(request: FastifyRequest, reply: FastifyReply): Promise<void> {
    const signed = request.cookies.SessionId;
    if (!signed) throw new UnauthorizedException();

    const session = reply.unsignCookie(signed);
    if (!session || !session.valid || !session.value)
      throw new UnauthorizedException();

    reply.clearCookie('SessionId');
    if (!(await this.sessions.get(session.value)))
      throw new UnauthorizedException();

    await this.sessions.deauthorize.byId(session.value);
  }

  async validateUser(user: EUser, password: string): Promise<boolean> {
    if (
      !user ||
      !user.password ||
      !(await argon.verify(password, user.password))
    )
      return false;
    return true;
  }

  async getUserAgent(request: FastifyRequest): Promise<IAuthAgent> {
    const header = request.headers['user-agent'];
    if (!header) {
      return {
        device: {
          agent: 'unknown',
          device: 'unknown',
        },
        ip: request.ip,
      };
    }
    const ua = await this.DD.detectAsync(header);

    return {
      device: {
        device: ua.device.type ?? 'unknown',
        agent: ua.client.name ?? 'unknown',
      },
      ip: request.ip,
    };
  }
}

export default new AuthService();
