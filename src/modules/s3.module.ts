import { config } from '../config';
import { S3 } from 'aws-sdk';

export const s3 = new S3({
  endpoint: config.s3.host,
  credentials: {
    accessKeyId: config.s3.key_id,
    secretAccessKey: config.s3.secret_key,
  },
  s3ForcePathStyle: true,
});

class S3Service {
  public client = s3;

  async checkIsFileAvailable(bucket: string, key: string): Promise<boolean> {
    try {
      await s3
        .headObject({
          Bucket: bucket,
          Key: key,
        })
        .promise();
      return true;
    } catch (error) {
      return false;
    }
  }
}
export default new S3Service();
