class timeUtils {
  getDate(
    mode: 'dmy' | 'my' | 'ymd' = 'dmy',
    separator: string = '.',
    date: Date = new Date()
  ) {
    if (mode == 'ymd') {
      return date.toISOString().slice(0, 10);
    }

    let d: any = date.getUTCDate();
    let m: any = date.getUTCMonth() + 1;
    let y: any = date.getUTCFullYear();

    if (d < 10) {
      d = `0${d}`;
    }

    if (m < 10) {
      m = `0${m}`;
    }

    if (mode === 'dmy') {
      return `${d}${separator}${m}${separator}${y}`;
    }
    if (mode === 'my') {
      return `${m}${separator}${y}`;
    } else {
      return `${m}${separator}${y}`;
    }
  }
}

export default new timeUtils();
