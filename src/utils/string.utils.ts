class String {
  getRandomString(options: {
    length: number;
    mode?: 'all' | 'numbers' | 'letters';
    case?: 'up' | 'low';
  }): string {
    const chars = {
      all: 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789',
      numbers: '0123456789',
      letters: 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm',
    };

    const length = options.length || 30;
    const mode = options.mode || 'all';

    let string = '';

    for (let i = 0; i < length; i++) {
      string += chars[mode].charAt(
        Math.floor(Math.random() * chars[mode].length)
      );
    }

    if (options.case === 'up') return string.toUpperCase();
    if (options.case === 'low') return string.toLowerCase();

    return string;
  }
}

export default new String();
