import { FastifyReply, FastifyRequest } from 'fastify';
import UnauthorizedException from '../../exceptions/unauthorized.exception';
import AuthService from '../../modules/auth/auth.service';
import BadRequestException from '../../exceptions/badrequest.exception';
import { repositories } from '../../modules/database.module';
import jwt from 'jsonwebtoken';
import { config } from '../../config';

interface IExtractedToken {
  type: string;
  token: string;
}

const extractTokenFromHeader = (header: string): IExtractedToken => {
  const [type, token] = header.split(' ');

  return { type, token };
};

export const requireAuthentication = async (
  request: FastifyRequest,
  reply: FastifyReply
): Promise<void> => {
  if (request.headers.authorization) {
    const token = extractTokenFromHeader(request.headers.authorization);

    if (token.type === 'Bearer') {
      try {
        const payload = jwt.verify(token.token, config.jwt.secret);

        if (payload.sub) {
          const user = await repositories.users.accounts.findOne({
            where: { id: Number(payload.sub) },
          });

          if (!user) throw new UnauthorizedException();

          request.user = user;
          return;
        }
      } catch (error) {}
    }
  }

  const signed = request.cookies.DigiProxySessionId;
  if (!signed) throw new UnauthorizedException();

  const { valid, value } = reply.unsignCookie(signed);
  if (!valid || !value) throw new UnauthorizedException();

  let sessionData = await AuthService.sessions.get(value);

  if (!sessionData) {
    reply.clearCookie('SessionId');
    throw new BadRequestException({
      error: 'invalid_session',
      description: 'Invalid session-id',
    });
  }

  const user = await repositories.users.accounts.findOne({
    where: { id: sessionData.user.id },
  });
  if (!user) {
    await AuthService.logout(request, reply);
    throw new UnauthorizedException();
  }

  request.user = user;
  request.session = value;
};

export const isNotAlreadyAuthorized = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  try {
    await requireAuthentication(request, reply);
  } catch (error) {
    return;
  }
  throw new BadRequestException({
    error: 'Already logged in',
    description: 'You already signed in',
  });
};
