import { FastifyReply, FastifyRequest } from 'fastify';
import { EUserRole } from '../../entities/user/EUser';
import ForbiddenException from '../../exceptions/forbidden.exception';
import { requireAuthentication } from './auth.middleware';

/**
 * @param role - required user role
 * @returns prehandler to require of role permissions
 */
export const requireRoleAuthentication = (role: EUserRole) => {
  return async (request: FastifyRequest, reply: FastifyReply) => {
    await requireAuthentication(request, reply);

    if (request.user.role !== role)
      throw new ForbiddenException({
        error: 'Forbidden',
        description: "You don't have access to do this action",
      });
  };
};
