import { EAuthToken } from 'src/entities/auth/EAuthToken';
import { EOAuthClient } from 'src/entities/oauth/EClient';
import { EOAuthToken } from 'src/entities/oauth/EToken';
import { EUser } from 'src/entities/user/EUser';

interface IError {
  error: string;
  description: string;
}

declare module 'fastify' {
  interface FastifyRequest {
    user: EUser;
    session: string;
  }

  interface FastifyReply {
    error: (error: IError) => void;
  }
}
