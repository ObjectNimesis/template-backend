FROM node:latest as dependencies

WORKDIR /identity-service
COPY package.json ./

RUN npm i

FROM node:latest as builder
WORKDIR /identity-service
COPY . .
COPY --from=dependencies /identity-service/node_modules ./node_modules
RUN npm run build

FROM node:latest as runner
WORKDIR /identity-service

COPY --from=builder /identity-service/ ./

ENV NODE_ENV production
ENV HTTP_HOST=0.0.0.0
ENV HTTP_PORT=3000

EXPOSE ${HTTP_PORT}
CMD npm run start